// <auto-generated />
namespace OnlineStore.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class reserved_When : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(reserved_When));
        
        string IMigrationMetadata.Id
        {
            get { return "201605171620198_reserved_When"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
