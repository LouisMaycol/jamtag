namespace OnlineStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_Price_TO_Double : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Sales", "Price", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Sales", "Price", c => c.Int(nullable: false));
        }
    }
}
