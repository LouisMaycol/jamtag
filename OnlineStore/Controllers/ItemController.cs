﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using OnlineStore.Helper;
using OnlineStore.Models;

namespace OnlineStore.Controllers
{
    public class ItemController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        int totaltake = 12;    //TOTAL TAKE
        #region Add Items for Sale

        // GET: AddItems
        [Authorize]
        public ActionResult AddItems()
        {
            //var listItems = new List<ListItem> 
            //{ 
            //      new ListItem { Text = "Mobile", Value = "Mobile" }, 
            //      new ListItem { Text = "Computers", Value = "Computers" },
            //      new ListItem { Text = "Pets", Value = "Pets" },
            //      new ListItem { Text = "Hobbies", Value = "Hobbies" },
            //      new ListItem { Text = "Motorcycle", Value = "Motorcycle" },
            //      new ListItem { Text = "Appliances", Value = "Appliances" },
            //      new ListItem { Text = "Furniture", Value = "Furniture" },
            //      new ListItem { Text = "Cars", Value = "Cars" },
            //       new ListItem { Text = "Clothes", Value = "Clothes" },
            //            new ListItem { Text = "Health", Value = "Health" },
            //                 new ListItem { Text = "Beauty", Value = "Beauty" }

            //};

            var listCondition = new List<ListItem>
            {
                new ListItem { Text = "Brand New", Value = "Brand New" },
                new ListItem { Text = "Secondhand", Value = "Secondhand" } 
            };

            //ViewBag.CategoryList = listItems;
            ViewBag.CategoryList = new SelectList(db.ItemType.OrderBy(m => m.ItemTypeName), "ItemTypeName", "ItemTypeName");
            ViewBag.ConditionList = listCondition;
            return View();
        }

        // POST: AddItems
        [HttpPost]
        [Authorize]
        public ActionResult AddItems(ItemsViewModel itemsViewModel, HttpPostedFileBase[] itemimages)
        {
            //Add value to master table
            ItemMaster master = new ItemMaster()
            {
                ItemName = itemsViewModel.ItemMaster.ItemName,
                DatePosted = DateTime.Now,
                UserId = User.Identity.GetUserId(),
                ContactNumber = itemsViewModel.ItemMaster.ContactNumber,
                EmailAddress = itemsViewModel.ItemMaster.EmailAddress,
                Location = itemsViewModel.ItemMaster.Location,
                IsApproved = "Draft",
                Availability = "Available"
            };
            db.ItemMaster.Add(master);

            ItemDetails details = new ItemDetails()
            {
                ItemDescription = itemsViewModel.ItemDetails.ItemDescription,
                ItemAmount = itemsViewModel.ItemDetails.ItemAmount,
                ItemCategory = itemsViewModel.ItemDetails.ItemCategory,
                ItemCondition = itemsViewModel.ItemDetails.ItemCondition,
                ItemMaster = master
            };

            db.ItemDetails.Add(details);
            db.SaveChanges();
            try
            {
                /*Loop for multiple files*/
                foreach (HttpPostedFileBase file in itemimages)
                {
                    ItemImages images = new ItemImages()
                    {
                        //Convert Images to byte to save to database
                        ItemPictures = CommonHelper.ConvertImageToByteArray(file),
                        ItemMaster = master
                    };

                    db.ItemImages.Add(images);
                    db.SaveChanges();
                }
            }
            catch
            {
                ViewBag.Message = "Error while uploading the files.";
            }

            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region View Items for Sale
        // GET: ViewItems

        public ActionResult ViewItems(string category, string condition, string itemName, int page = 1)
        {
            #region populate dropdown
            var listCondition = new List<ListItem>
            {
                new ListItem { Text = "Brand New", Value = "Brand New" },
                new ListItem { Text = "Secondhand", Value = "Secondhand" } 
            };


            ViewBag.CategoryList = new SelectList(db.ItemType.OrderBy(m => m.ItemTypeName), "ItemTypeName", "ItemTypeName");
            ViewBag.ConditionList = new SelectList(listCondition, "Text", "Text");
    

            if (page == 0)
            {
                page = 1;
            }


            #endregion

            //List<ItemsViewModel> ItemsVm = new List<ItemsViewModel>();
            var ItemsVm = new List<ItemForSaleViewModel>();
            var dba = db.ItemMaster.Where(m=>m.Availability != "Reserved");
            var master = dba.ToList();
            if (!String.IsNullOrEmpty(itemName))
            {
                master = dba.Where(m => m.ItemName.Contains(itemName)).ToList();
            }


            foreach (var item in master)
            {

                bool isPrem;
                var checker = db.PremiumUsers.Where(m => m.UserId == item.UserId).FirstOrDefault();

                if (checker == null)
                {
                    isPrem = false;
                }
                else
                {
                    if (checker.IsPremium == true)
                    {
                        isPrem = true;
                    }
                    else
                    {
                        isPrem = false;
                    }
                }

                var temp = new ItemForSaleViewModel()
                {
                    ItemName = item.ItemName,
                    defaultImage = db.ItemImages.Where(m => m.ItemMaster.ItemId == item.ItemId).FirstOrDefault().ItemPictures,
                    ItemPrice = db.ItemDetails.Where(m => m.ItemMaster.ItemId == item.ItemId).FirstOrDefault().ItemAmount,
                    ItemMasterId = item.ItemId,
                    category = db.ItemDetails.Where(m => m.ItemMaster.ItemId == item.ItemId).FirstOrDefault().ItemCategory,
                    condition = db.ItemDetails.Where(m => m.ItemMaster.ItemId == item.ItemId).FirstOrDefault().ItemCondition,
                    isPremium = isPrem,
                    isApproved = item.IsApproved
                };
                ItemsVm.Add(temp);

            }
            //THISSSSSSS
            if (!String.IsNullOrEmpty(category) && !String.IsNullOrEmpty(condition))
            {
                var model = ItemsVm.Where(m => m.category == category).Where(m => m.condition == condition).Where(m => m.isApproved == "Approved").Where(m => m.Availability == "Available").ToList();

                ViewBag.PageCount = new SelectList(generatePageList(getPage(model.Count())), "Text", "Text");

                if (page == 1)
                {
                    int totalskip = page * totaltake;
                    return View(model.Take(totaltake).ToList());
                }
                else if (page == 2)
                {
                    int totalskip = totaltake;
                    return View(model.Skip(totalskip).Take(totaltake).ToList());
                }
                else
                {
                    int totalskip = (page - 1) * totaltake;
                    return View(model.Skip(totalskip).Take(totaltake).ToList());
                }
            }
            else if (!String.IsNullOrEmpty(category) && String.IsNullOrEmpty(condition))
            {
                var model = ItemsVm.Where(m => m.category == category).Where(m => m.isApproved == "Approved").Where(m => m.Availability != "Available").ToList();
                ViewBag.PageCount = new SelectList(generatePageList(getPage(model.Count())), "Text", "Text");



                if (page == 1)
                {
                    int totalskip = page * totaltake;
                    return View(model.Take(totaltake).ToList());
                }
                else if (page == 2)
                {
                    int totalskip = totaltake;
                    return View(model.Skip(totalskip).Take(totaltake).ToList());
                }
                else
                {
                    int totalskip = (page - 1) * totaltake;
                    return View(model.Skip(totalskip).Take(totaltake).ToList());
                }
            }
            else if (String.IsNullOrEmpty(category) && !String.IsNullOrEmpty(condition))
            {
                var model = ItemsVm.Where(m => m.condition == condition).Where(m => m.isApproved == "Approved").Where(m => m.Availability != "Sold").ToList();
                ViewBag.PageCount = new SelectList(generatePageList(getPage(model.Count())), "Text", "Text");



                if (page == 1)
                {
                    int totalskip = page * totaltake;
                    return View(model.Take(totaltake).ToList());
                }
                else if (page == 2)
                {
                    int totalskip = totaltake;
                    return View(model.Skip(totalskip).Take(totaltake).ToList());
                }
                else
                {
                    int totalskip = (page - 1) * totaltake;
                    return View(model.Skip(totalskip).Take(totaltake).ToList());
                }

            }
            else
            {
                var model = ItemsVm.OrderBy(m => m.isPremium).Where(m => m.isApproved == "Approved").Where(m => m.Availability != "Sold").ToList();

                ViewBag.PageCount = new SelectList(generatePageList(getPage(ItemsVm.Where(m => m.isApproved == "Approved").Where(m => m.Availability != "Sold").Count())), "Text", "Text");

                if (page == 1)
                {
                    int totalskip = page * totaltake;
                    return View(model.Take(totaltake).ToList());
                }
                else if (page == 2)
                {
                    int totalskip = totaltake;
                    return View(model.Skip(totalskip).Take(totaltake).ToList());
                }
                else
                {
                    int totalskip = (page - 1) * totaltake;
                    return View(model.Skip(totalskip).Take(totaltake).ToList());
                }

            }
        }

        public int getPage(int modelCount)
        {

            int totalpage = modelCount / totaltake;
            int rem = modelCount % totaltake;
            if (rem != 0)
            {
                totalpage = totalpage + 1;
            }
            return totalpage;
        }

        public List<ListItem> generatePageList(int totalpagecount)
        {

            List<ListItem> pagecount = new List<ListItem>();
            for (var i = 1; i <= totalpagecount; i++)
            {
                ListItem temp = new ListItem();
                temp.Text = i.ToString();
                temp.Value = i.ToString();
                pagecount.Add(temp);
            }
            return pagecount;
        }

        #endregion

        #region Filter Items for Sale
        public ActionResult FilterItemsByCategory(string category)
        {
            var viewModels = new List<ItemsViewModel>();

            var details = db.ItemDetails.Where(m => m.ItemCategory == category).ToList();

            foreach (var dtls in details)
            {
                var mst = db.ItemMaster.FirstOrDefault(m => m.ItemId == dtls.ItemMaster.ItemId);

                var images = db.ItemImages.Where(m => m.ItemMaster.ItemId == mst.ItemId).ToList();

                var vm = new ItemsViewModel()
                {
                    ItemMaster = mst,
                    ItemDetails = dtls,
                    ItemImages = images
                };

                viewModels.Add(vm);
            }

            //var listItems = new List<ListItem> 
            //{ 
            //      new ListItem { Text = "Mobile", Value = "Mobile" }, 
            //      new ListItem { Text = "Computers", Value = "Computers" },
            //      new ListItem { Text = "Pets", Value = "Pets" },
            //      new ListItem { Text = "Hobbies", Value = "Hobbies" },
            //      new ListItem { Text = "Motorcycle", Value = "Motorcycle" },
            //      new ListItem { Text = "Appliances", Value = "Appliances" },
            //      new ListItem { Text = "Furniture", Value = "Furniture" },
            //      new ListItem { Text = "Cars", Value = "Cars" } ,
            //                     new ListItem { Text = "Clothes", Value = "Clothes" } ,
            //                            new ListItem { Text = "Health", Value = "Health" },
            //                 new ListItem { Text = "Beauty", Value = "Beauty" }

            //};

            var listCondition = new List<ListItem>
            {
                new ListItem { Text = "Brand New", Value = "Brand New" },
                new ListItem { Text = "Secondhand", Value = "Secondhand" } 
            };

            //    ViewBag.CategoryList = listItems;
            ViewBag.CategoryList = new SelectList(db.ItemType.OrderBy(m => m.ItemTypeName), "ItemTypeName", "ItemTypeName");
            ViewBag.ConditionList = listCondition;

            TempData["category-list"] = viewModels;

            return RedirectToAction("ViewItems", new { filter = category });
        }

        public ActionResult FilterItemsByCondition(string condition)
        {
            var viewModels = new List<ItemsViewModel>();

            var details = db.ItemDetails.Where(m => m.ItemCondition == condition).ToList();

            foreach (var dtls in details)
            {
                var mst = db.ItemMaster.FirstOrDefault(m => m.ItemId == dtls.ItemMaster.ItemId);

                var images = db.ItemImages.Where(m => m.ItemMaster.ItemId == mst.ItemId).ToList();

                var vm = new ItemsViewModel()
                {
                    ItemMaster = mst,
                    ItemDetails = dtls,
                    ItemImages = images
                };

                viewModels.Add(vm);
            }

            //var listItems = new List<ListItem> 
            //{ 
            //      new ListItem { Text = "Mobile", Value = "Mobile" }, 
            //      new ListItem { Text = "Computers", Value = "Computers" },
            //      new ListItem { Text = "Pets", Value = "Pets" },
            //      new ListItem { Text = "Hobbies", Value = "Hobbies" },
            //      new ListItem { Text = "Motorcycle", Value = "Motorcycle" },
            //      new ListItem { Text = "Appliances", Value = "Appliances" },
            //      new ListItem { Text = "Furniture", Value = "Furniture" },
            //      new ListItem { Text = "Cars", Value = "Cars" } ,
            //                     new ListItem { Text = "Clothes", Value = "Clothes" } ,
            //                            new ListItem { Text = "Health", Value = "Health" },
            //                 new ListItem { Text = "Beauty", Value = "Beauty" }
            //};

            var listCondition = new List<ListItem>
            {
                new ListItem { Text = "Brand New", Value = "Brand New" },
                new ListItem { Text = "Secondhand", Value = "Secondhand" } 
            };

            //ViewBag.CategoryList = listItems;
            ViewBag.CategoryList = new SelectList(db.ItemType.OrderBy(m => m.ItemTypeName), "ItemTypeName", "ItemTypeName");
            ViewBag.ConditionList = listCondition;

            TempData["category-list"] = viewModels;

            return RedirectToAction("ViewItems", new { filter = condition });
        }


        #endregion

        #region View Detailed Items for Sale
        // GET: ItemForSale
        public ActionResult ItemForSale(int id)
        
        {
            var details = getItemDetails(id);

            var ratings = db.UserRatings.Where(m => m.UserId == details.ItemMaster.UserId).ToList();

            int totalScore = ratings.Sum(rates => rates.Score);

            string averageRating = ratings.Count == 0 ? totalScore.ToString("#,##0.00") : ((double)totalScore / ratings.Count).ToString("##,##0.00");

            ViewBag.AverageRating = averageRating;

            var master = db.ItemMaster.Where(m => m.ItemId == details.ItemMaster.ItemId).FirstOrDefault();

            master.ViewCount = master.ViewCount + 1;

            db.SaveChanges();



            return View(getItemDetails(id));
        }

        #endregion

        #region Edit Items for Sale

        // GET: EditItemForSale
        [Authorize]
        public ActionResult EditItemForSale(int id)
        {


            var listCondition = new List<ListItem>
            {
                new ListItem { Text = "Brand New", Value = "Brand New" },
                new ListItem { Text = "Secondhand", Value = "Secondhand" } 
            };

            var Availability = new List<ListItem>
            {
                new ListItem { Text = "Sold", Value = "Sold" },
                new ListItem { Text = "Available", Value = "Available" } ,
                new ListItem { Text = "Reserved", Value = "Reserved" } 
            };

            var model = getItemDetails(id);

            //ViewBag.CategoryList = listItems;
            ViewBag.CategoryList = new SelectList(db.ItemType.OrderBy(m => m.ItemTypeName), "ItemTypeName", "ItemTypeName");
            ViewBag.ConditionList = listCondition;
            ViewBag.AvailabilityList = Availability;
            ViewBag.email = db.Users.Where(m => m.Id == model.ItemMaster.reservedBy).FirstOrDefault().Email;

            return View(model);
        }

        // POST: EditItemForSale
        [HttpPost]
        [Authorize]
        public ActionResult EditItemForSale(ItemsViewModel viewModel, HttpPostedFileBase[] itemimages)
        {
            var Availability = new List<ListItem>
            {
                new ListItem { Text = "Sold", Value = "Sold" },
                new ListItem { Text = "Available", Value = "Available" },
                new ListItem { Text = "Reserved", Value = "Reserved" } 
            };

            var listCondition = new List<ListItem>
            {
                new ListItem { Text = "Brand New", Value = "Brand New" },
                new ListItem { Text = "Secondhand", Value = "Secondhand" } 
            };
            if (viewModel.ItemMaster.Availability == "Available")
            {
                viewModel.ItemMaster.IsApproved = "Draft";
                viewModel.ItemMaster.reservedBy = "";
                viewModel.ItemMaster.isReserved = "false";
                viewModel.ItemMaster.ReservedWhen = null;
            }
            db.Entry(viewModel.ItemMaster).State = EntityState.Modified;
            db.Entry(viewModel.ItemDetails).State = EntityState.Modified;

            try
            {
                /*Loop for multiple files*/
                foreach (HttpPostedFileBase file in itemimages)
                {
                    ItemImages images = new ItemImages()
                    {
                        //Convert Images to byte to save to database
                        ItemPictures = CommonHelper.ConvertImageToByteArray(file),
                        ItemMaster = viewModel.ItemMaster
                    };

                    db.ItemImages.Add(images);
                    db.SaveChanges();
                }
            }
            catch
            {
                ViewBag.Message = "Error while uploading the files.";
            }

            if (viewModel.ItemMaster.Availability == "Sold")
            {
                Sales item = new Sales
                {
                    DateSold = DateTime.Today,
                    Item = viewModel.ItemMaster.ItemName,
                    Price = db.ItemDetails.Where(m => m.ItemMaster.ItemId == viewModel.ItemMaster.ItemId).FirstOrDefault().ItemAmount,
                    UserID = viewModel.ItemMaster.UserId
                };
                db.Sales.Add(item);
                db.SaveChanges();
            }

            //ViewBag.CategoryList = listItems;
            ViewBag.CategoryList = new SelectList(db.ItemType.OrderBy(m => m.ItemTypeName), "ItemTypeName", "ItemTypeName");
            ViewBag.ConditionList = listCondition;
            ViewBag.AvailabilityList = Availability;
            db.SaveChanges();

            return View(getItemDetails(viewModel.ItemMaster.ItemId));
        }

        #endregion

        #region Delete Item
        public void DeleteItem(int id)
        {
            var itemDetails = db.ItemDetails.Where(m => m.ItemMaster.ItemId == id).FirstOrDefault();
            var itemPic = db.ItemImages.Where(m => m.ItemMaster.ItemId == id).FirstOrDefault();
            var itemMaster = db.ItemMaster.Where(m => m.ItemId == id).FirstOrDefault();

            db.ItemDetails.Remove(itemDetails);
            db.ItemImages.Remove(itemPic);
            db.ItemMaster.Remove(itemMaster);

            db.SaveChanges();

        }
        #endregion

        #region View My Items for Sale
        // GET: DisplayMyItems
        [Authorize]
        public ActionResult DisplayMyItems()
        //{
        //    var userId = User.Identity.GetUserId();

        //    return View(getMyItems(userId));
        //}
        {
            var userId = User.Identity.GetUserId();
            var model = new List<MyItemsViewModel>();
            var itemMaster = db.ItemMaster.Where(m => m.UserId == userId).FirstOrDefault();
            //model.defaultImage = Convert.ToBase64String(db.ItemImages.Where(m => m.ItemMaster.UserId == itemMaster.UserId).FirstOrDefault().ItemPictures);
            //model.ItemMasterList = db.ItemMaster.Where(m => m.UserId == itemMaster.UserId).ToList();
            //model.ItemImages = db.ItemImages.Where(m => m.ItemMaster.UserId == itemMaster.UserId).ToList();
            //model.ItemDetailsList = db.ItemDetails.Where(m => m.ItemMaster.UserId == itemMaster.UserId).ToList();
            var masterlist = db.ItemMaster.Where(m => m.UserId == userId).ToList();

            foreach (var item in masterlist)
            {
                var temp = new MyItemsViewModel()
                {
                    ItemName = item.ItemName,
                    defaultImage = db.ItemImages.Where(m => m.ItemMaster.ItemId == item.ItemId).FirstOrDefault().ItemPictures,
                    ItemPrice = db.ItemDetails.Where(m => m.ItemMaster.ItemId == item.ItemId).FirstOrDefault().ItemAmount,
                    ItemMasterId = item.ItemId,

                };
                model.Add(temp);
            }
            return View(model);
        }

        #endregion

        #region User Rating

        public string RateUser(int score, string userId)
        {
            UserRating rating = new UserRating()
            {
                Score = score,
                UserId = userId,
                DateRated = DateTime.Now
            };

            db.UserRatings.Add(rating);
            db.SaveChanges();

            var ratings = db.UserRatings.Where(m => m.UserId == userId).ToList();

            int totalScore = ratings.Sum(rates => rates.Score);

            return ratings.Count == 0 ? totalScore.ToString("#,##0.00") : ((double)totalScore / ratings.Count).ToString("##,##0.00");
        }

        #endregion

        #region User Feedback
        [Authorize]
        public void AddFeedback(string feedback, string userId)
        {
            Feedback feedbackitem = new Feedback()
            {
                DateTimeCreated = DateTime.Now,
                FeedbackText = feedback,
                UserId = userId
            };
            db.Feedback.Add(feedbackitem);
            db.SaveChanges();
        }
        #endregion

        #region Approve Item
        [HttpGet]
        [Authorize]
        public ActionResult ItemsToApprove()
        {

            var identity = User.Identity.GetUserId();
            var user = db.Users.Where(m => m.Id == identity).FirstOrDefault();
            if (user.IsAdmin == false)
            {
                return RedirectToAction("Index", "Home");
            }
            var itemList = db.ItemMaster.Where(m => m.IsApproved == "Draft").ToList();
            var detailList = db.ItemDetails.ToList();
            List<ItemForSaleViewModel> vm = new List<ItemForSaleViewModel>();
            foreach (var item in itemList)
            {
                var temp = new ItemForSaleViewModel()
                {
                    defaultImage = db.ItemImages.Where(m => m.ItemMaster.ItemId == item.ItemId).FirstOrDefault().ItemPictures,
                    ItemPrice = db.ItemDetails.Where(m => m.ItemMaster.ItemId == item.ItemId).FirstOrDefault().ItemAmount,
                    ItemName = item.ItemName,
                    ItemMasterId = item.ItemId,
                    category = db.ItemDetails.Where(m => m.ItemMaster.ItemId == item.ItemId).FirstOrDefault().ItemCategory,
                    condition = db.ItemDetails.Where(m => m.ItemMaster.ItemId == item.ItemId).FirstOrDefault().ItemCondition,
                    UserId = String.Format("{0} {1}", db.Users.Where(m => m.Id == item.UserId).FirstOrDefault().FirstName, db.Users.Where(m => m.Id == item.UserId).FirstOrDefault().LastName)
                };
                vm.Add(temp);
            }

            return View(vm);
        }

        public void ApproveItem(int itemNumber)
        {
            var item = db.ItemMaster.Find(itemNumber);
            item.IsApproved = "Approved";


            AdminReport AR = new AdminReport();
            AR.DateApproved = DateTime.Now;
            AR.ItemMasterID = item.ItemId;
            AR.ItemName = item.ItemName;
            AR.Profit = db.ItemDetails.Where(m => m.ItemMaster.ItemId == item.ItemId).FirstOrDefault().ItemAmount * .05;
            AR.UserName = db.Users.Where(m => m.Id == item.UserId).FirstOrDefault().UserName;

            db.AdminReport.Add(AR);

            db.SaveChanges();
        }

        public void DisapproveItem(int itemNumber)
        {
            var item = db.ItemMaster.Find(itemNumber);
            item.IsApproved = "Disapproved";
            db.SaveChanges();
        }
        #endregion

        public void ReserveItem(int id)
        {
            var item = db.ItemMaster.Where(m => m.ItemId == id).FirstOrDefault();

            item.isReserved = "true";
            item.reservedBy = User.Identity.GetUserId();
            item.ReservedWhen = DateTime.Now;
            item.Availability = "Reserved";

            db.SaveChanges();
        }

        #region Function Helpers

        //Reusable code to get item details
        public ItemsViewModel getItemDetails(int id)
        {
            var mst = new ItemMaster();
            var dtls = new ItemDetails();
            var imgs = new List<ItemImages>();
            var feed = new List<Feedback>();

            mst = db.ItemMaster.FirstOrDefault(m => m.ItemId == id);
            dtls = db.ItemDetails.FirstOrDefault(m => m.ItemMaster.ItemId == id);
            imgs = db.ItemImages.Where(m => m.ItemMaster.ItemId == id).ToList();
            feed = db.Feedback.Where(m => m.UserId == mst.UserId).ToList();
            var vm = new ItemsViewModel()
            {
                ItemMaster = mst,
                ItemDetails = dtls,
                ItemImages = imgs,
                FeedbackList = feed
            };

            return vm;
        }

        //public List<MyItemsViewModel> getMyItems(string id)
        //{
        //    var imgs = new List<ItemImages>();
        //    var dlist = new List<ItemDetails>();
        //    var mlist = new List<ItemMaster>();

        //    imgs = db.ItemImages.Where(m => m.ItemMaster.UserId == id).ToList();
        //    dlist = db.ItemDetails.Where(m => m.ItemMaster.UserId == id).ToList();
        //    mlist = db.ItemMaster.Where(m => m.UserId == id).ToList();

        //    var vm = new MyItemsViewModel
        //    {
        //        ItemImages = imgs,
        //        ItemDetailsList = dlist,
        //        ItemMasterList = mlist,       
        //    };



        //    return vm;
        //}

        //Endpoint to delete image
        public void DeleteImage(int id)
        {
            var imageToDelete = db.ItemImages.Find(id);
            db.ItemImages.Remove(imageToDelete);
            db.SaveChanges();
        }

        #endregion

        #region Delete Coment
        public void DeleteComment(int feedbackId)
        {
            var feedback = db.Feedback.Where(m => m.FeedbackID == feedbackId).FirstOrDefault();
            var user = User.Identity.GetUserId();
            db.Feedback.Remove(feedback);
            db.SaveChanges();

        }
        #endregion

    }
}