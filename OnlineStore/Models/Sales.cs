﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class Sales
    {
        [Key]
        public int SalesID { get; set; }
        public string UserID { get; set; }
        public string Item { get; set; }
        public double Price { get; set; }
        public DateTime? DateSold { get; set; }

    }
    public class SalesViewModel
    {
        public double TotalSales { get; set; }
        public int TotalItemSold { get; set; }
        public string Month { get; set; }
    }
}