﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class AdminReport
    {
        public int AdminReportID { get; set; }
        public string ItemName { get; set; }
        public int ItemMasterID { get; set; }
        public string UserName { get; set; }
        public DateTime? DateApproved { get; set; }
        public double Profit { get; set; }
    }

}