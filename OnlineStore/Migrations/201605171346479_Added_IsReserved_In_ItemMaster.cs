namespace OnlineStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_IsReserved_In_ItemMaster : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemMasters", "isReserved", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemMasters", "isReserved");
        }
    }
}
