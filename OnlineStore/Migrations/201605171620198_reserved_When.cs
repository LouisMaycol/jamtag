namespace OnlineStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reserved_When : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemMasters", "ReservedWhen", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemMasters", "ReservedWhen");
        }
    }
}
