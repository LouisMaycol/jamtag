﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class PremiumUsers
    {
        [Key]
        public int PremiumUserId { get; set; }
        public string UserId { get; set; }
        public bool IsPremium { get; set; }
    }
}