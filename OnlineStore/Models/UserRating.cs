﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class UserRating
    {
        [Key]
        public int RatingId { get; set; }
        public string UserId { get; set; }
        public int Score { get; set; }
        public DateTime? DateRated { get; set; }
    }
}