﻿using OnlineStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineStore.Controllers
{
    public class AdminReportController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: AdminReport
        public ActionResult AdminReport()
        {
            List<AdminReport> model = new List<AdminReport>();

            var ReportList = db.AdminReport.ToList();

            model = ReportList;

            return View(model);
        }

        public void WebViewCount()
        {
            if (db.ViewCounter.Count() == 0)
            {
                ViewCounter counter = new ViewCounter();
                counter.TotalViews = 1;
                db.ViewCounter.Add(counter);
            }else
            {
                var model = db.ViewCounter.FirstOrDefault();
                model.TotalViews = model.TotalViews + 1;
            }

            db.SaveChanges();
        }

        public int getView()
        {
            return db.ViewCounter.FirstOrDefault().TotalViews;
        }
    }
}