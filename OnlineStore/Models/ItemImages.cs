﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class ItemImages
    {
        [Key]
        public int Id { get; set; }
        public byte[] ItemPictures { get; set; }
        public virtual ItemMaster ItemMaster { get; set; }
    }
}