﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class ItemsViewModel
    {
        public ItemMaster ItemMaster { get; set; }
        public ItemDetails ItemDetails { get; set; }
        public List<ItemImages> ItemImages { get; set; }
        public List<Feedback> FeedbackList { get; set; }
    }

    public class MyItemsViewModel
    {
        public string ItemName { get; set;}
        public byte[] defaultImage { get; set; }
        public double ItemPrice {get;set;}
        public int ItemMasterId { get; set; }

        //public List<ItemImages> ItemImages { get; set; }
        //public List<ItemDetails> ItemDetailsList { get; set; }
        //public List<ItemMaster> ItemMasterList { get; set; }
    }

    public class ItemForSaleViewModel
    {
        public string ItemName { get; set; }
        public byte[] defaultImage { get; set; }
        public double ItemPrice { get; set; }
        public int ItemMasterId { get; set; }
        public string category { get; set; }
        public string condition { get; set; }
        public bool isPremium { get; set; }
        public string UserId { get; set; }
        public string isApproved { get; set; }

        public string Availability { get; set; }
    }
}