﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class Feedback
    {
        [Key]
        public int FeedbackID { get; set; }
        public string UserId { get; set; }
        public string FeedbackText { get; set; }
        public DateTime? DateTimeCreated { get; set; }
    }
}