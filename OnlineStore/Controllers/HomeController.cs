﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using OnlineStore.Models;

namespace OnlineStore.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
           var user =  User.Identity.GetUserId();
            bool isLogged;
            if(user != null)
            {
                isLogged = true;
                ViewBag.IsAdmin = db.Users.Where(m => m.Id == user).FirstOrDefault().IsAdmin;
            }
            else
            {
                isLogged = false;
            }       
            ViewBag.IsLoggedIn = isLogged;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Help()
        {
            return View();
        }
        [Authorize]
        public ActionResult Chat()
        {
            var user = User.Identity.GetUserId();
            var email = db.Users.Where(m => m.Id == user).FirstOrDefault().Email; 
            ViewBag.Email = email;
            return View();
        }
    }
}