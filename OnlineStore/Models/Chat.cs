﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class Chat
    {
        public int id { get; set; }
        public string User { get; set; }
        public string message { get; set; }
        public DateTime DateTime { get; set; }
    }
}