﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class ItemDetails
    {
        [Key]
        public int ItemDetailId { get; set; }
        public string ItemDescription { get; set; }
        public double ItemAmount { get; set; }
        public string ItemCondition { get; set; }
        public string ItemCategory { get; set; }

        public virtual ItemMaster ItemMaster { get; set; }

    }
}