﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace OnlineStore.Helper
{
    public static class CommonHelper
    {
        public static byte[] ConvertImageToByteArray(HttpPostedFileBase file)
        {
            byte[] convertedToBytes = null;
            BinaryReader reader = new BinaryReader(file.InputStream);
            convertedToBytes = reader.ReadBytes((int)file.ContentLength);

            return convertedToBytes;
        }
    }
}