namespace OnlineStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ADd_HOmepage_COunter : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ViewCounters",
                c => new
                    {
                        ViewCounterID = c.Int(nullable: false, identity: true),
                        TotalViews = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ViewCounterID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ViewCounters");
        }
    }
}
