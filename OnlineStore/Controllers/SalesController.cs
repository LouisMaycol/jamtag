﻿using OnlineStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace OnlineStore.Controllers
{
    public class SalesController : Controller
    {
        // GET: Sales
        ApplicationDbContext db = new ApplicationDbContext();
        public JsonResult GetSales()
        {
            var user = User.Identity.GetUserId();
            var year = DateTime.Today.Year;
            string[] Months = new string[12] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            int arraycount = Months.Count();
            List<SalesViewModel> sales = new List<SalesViewModel>();
            var salesList = db.Sales.ToList();

            for (int i = 1; i <= arraycount; i++)
            {
                string minDay = year + "/" + i + "/01";
                string maxDay = year + "/" + i + "/" + DateTime.DaysInMonth(year, i);
                DateTime parsedMin = DateTime.Parse(minDay);
                DateTime parsedMax = DateTime.Parse(maxDay);
                var salesItems = db.Sales.Where(m => m.DateSold >= parsedMin && m.DateSold <= parsedMax && m.UserID == user).ToList();
                SalesViewModel temp = new SalesViewModel();
                temp.Month = Months[i - 1];
                temp.TotalItemSold = salesItems.Count();
                temp.TotalSales = salesItems.Sum(m => m.Price);
                sales.Add(temp);
            }
            //foreach (var item in Months)
            //{
            //    if()
            //    {

            //       var salesItems = db.Sales.Where(m=>m.DateSold <= DateTime.Parse(year+"/01/01") && m.DateSold >= DateTime.Parse(year+"/01/31")).ToList();
            //            SalesViewModel temp = new SalesViewModel();
            //            temp.Month = item;
            //            temp.TotalItemSold = salesItems.Count();
            //            temp.TotalSales = salesItems.Sum(m => m.Price);
            //            sales.Add(temp);
            //    }
            //    else if(item == "February")
            //    {

            //    }

            //}


            return Json(new { sales }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSalesProfit()
        {
            var year = DateTime.Today.Year;
            string[] Months = new string[12] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            int arraycount = Months.Count();
            List<SalesViewModel> sales = new List<SalesViewModel>();
            var adminList = db.AdminReport.ToList();

            for (int i = 1; i <= arraycount; i++)
            {
                string minDay = year + "/" + i + "/01";
                string maxDay = year + "/" + i + "/" + DateTime.DaysInMonth(year, i);
                DateTime parsedMin = DateTime.Parse(minDay);
                DateTime parsedMax = DateTime.Parse(maxDay);
                var adminItem = db.AdminReport.Where(m => m.DateApproved >= parsedMin && m.DateApproved <= parsedMax).ToList();
                SalesViewModel temp = new SalesViewModel();
                temp.Month = Months[i - 1];
                temp.TotalItemSold = adminItem.Count();
                temp.TotalSales = adminItem.Sum(m => m.Profit);
                sales.Add(temp);
            }

            return Json(new { sales }, JsonRequestBehavior.AllowGet);
        }

    }
}