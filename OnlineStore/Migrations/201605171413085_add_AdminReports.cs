namespace OnlineStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_AdminReports : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdminReports",
                c => new
                    {
                        AdminReportID = c.Int(nullable: false, identity: true),
                        ItemName = c.String(),
                        ItemMasterID = c.Int(nullable: false),
                        UserName = c.String(),
                        DateApproved = c.DateTime(),
                        Profit = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.AdminReportID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AdminReports");
        }
    }
}
