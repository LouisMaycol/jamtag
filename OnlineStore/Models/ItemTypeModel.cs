﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class ItemType
    {
        [Key]
        public int ItemTypeID { get; set; }
        public string ItemTypeName { get; set; }
    }
}