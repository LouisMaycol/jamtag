namespace OnlineStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ItemType_Model : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ItemTypes",
                c => new
                    {
                        ItemTypeID = c.Int(nullable: false, identity: true),
                        ItemTypeName = c.String(),
                    })
                .PrimaryKey(t => t.ItemTypeID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ItemTypes");
        }
    }
}
