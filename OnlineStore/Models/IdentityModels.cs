﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace OnlineStore.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool IsPremium { get; set; }
        public bool IsEmailConfirmed { get; set; }
        public bool IsAdmin { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<ItemMaster> ItemMaster { get; set; }
        public DbSet<ItemDetails> ItemDetails { get; set; }
        public DbSet<ItemImages> ItemImages { get; set; }
        public DbSet<UserRating> UserRatings { get; set; }
        public DbSet<PremiumUsers> PremiumUsers { get; set; }
        public DbSet<Feedback> Feedback { get; set; }
        public DbSet<Chat> Chat { get; set; }
        public DbSet<ItemType> ItemType { get; set; }
        public DbSet<Sales> Sales { get; set; }

        public DbSet<AdminReport> AdminReport { get; set; }

        public DbSet<ViewCounter> ViewCounter { get; set; }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}