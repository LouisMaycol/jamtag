namespace OnlineStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_ReservedBy_ItemMaster : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemMasters", "reservedBy", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemMasters", "reservedBy");
        }
    }
}
