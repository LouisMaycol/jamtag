namespace OnlineStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SalesReal : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sales",
                c => new
                    {
                        SalesID = c.Int(nullable: false, identity: true),
                        UserID = c.String(),
                        Item = c.String(),
                        Price = c.Int(nullable: false),
                        DateSold = c.DateTime(),
                    })
                .PrimaryKey(t => t.SalesID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Sales");
        }
    }
}
