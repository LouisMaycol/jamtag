namespace OnlineStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ViewCount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemMasters", "ViewCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemMasters", "ViewCount");
        }
    }
}
