﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class ViewCounter
    {
        public int ViewCounterID { get; set; }
        public int TotalViews { get; set; }
    }
}