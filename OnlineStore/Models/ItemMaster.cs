﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineStore.Models
{
    public class ItemMaster
    {
        [Key]
        public int ItemId { get; set; }
        [Required]
        public string ItemName { get; set; }
        public DateTime? DatePosted { get; set; }
        public string UserId { get; set; }
        [Required]
        public string ContactNumber { get; set; }
        [Required]
        public string EmailAddress { get; set; }
        [Required]
        public string Location { get; set; }       
        public string IsApproved { get; set; }

        public string Availability { get; set; }

        public int ViewCount { get; set; }

        public string isReserved { get; set; }
        public DateTime? ReservedWhen { get; set; }
        public string reservedBy { get; set; }

        public ICollection<ItemDetails> ItemDetail { get; set; }
        public ICollection<ItemImages> ItemImage { get; set; } 
    }
}