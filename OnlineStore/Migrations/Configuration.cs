namespace OnlineStore.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using OnlineStore.Models;
    using Microsoft.AspNet.Identity;

    internal sealed class Configuration : DbMigrationsConfiguration<OnlineStore.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(OnlineStore.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var passwordHash = new PasswordHasher();
            string password = passwordHash.HashPassword("P@ssw0rd");
            string usern = "Adm_JamTag0nline@Jamtag.com";
            if (!(context.Users.Any(m => m.UserName == usern)))
            {
                context.Users.AddOrUpdate(m => m.UserName, new ApplicationUser
                {
                    UserName = usern,
                    Email = usern,
                    PasswordHash = password,
                    IsAdmin = true,
                    FirstName = "Admin",
                    LastName = "Admin",
                    SecurityStamp = "J23sdofasjkl34"
                });
                context.SaveChanges();
            }

            //context.ItemType.AddOrUpdate(
            //    m => m.ItemTypeName,
            //        new ItemType { ItemTypeName = "Mobile" },
            //        new ItemType { ItemTypeName = "Computers" },
            //        new ItemType { ItemTypeName = "Pets" },
            //        new ItemType { ItemTypeName = "Hobbies" },
            //        new ItemType { ItemTypeName = "Motorcycle" },
            //        new ItemType { ItemTypeName = "Appliances" },
            //        new ItemType { ItemTypeName = "Furniture" },
            //        new ItemType { ItemTypeName = "Cars" },
            //        new ItemType { ItemTypeName = "Clothes" },
            //        new ItemType { ItemTypeName = "Accessories" }
            //    );

        }
    }
}
